///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Aksel Sloan <aksel@hawaii.edu>
/// @date    23rd January 2022 
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_MAX_NUMBER (2048)

int theMaxValue = DEFAULT_MAX_NUMBER;
int main( int argc, char* argv[] ) {
 
   printf( "Cat `n Mouse\n" );
       
///////////////////////////////////////////////////////////
///////////////Making a new max number based on user input 
////////////////////////////////////////////////////////////
   if (argc > 1)       //Runs if the user inputs a number
      {
      int userMax = atoi( argv[1] );
  
      if (32767 > userMax && userMax >= 1) //if valid we have a new max value 
         {
         theMaxValue = userMax;  
     
         }

      else
         {
          printf("Your input was invalid, use a value between 1 and 32767\n");
          return 1;
         }
      }  

   
////////////////////////////////////////////////////////////
//
   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
  
 
   ///
   //generate a random number
   /// 
   srand(time(0));
   int theNumberImThinkingOf = (rand() % (theMaxValue)) + 1;  
//   printf("Random number picked by the mouse: %d\n",theNumberImThinkingOf);
  //////////
  //playing the actual game
  /////////
   int aGuess;    
   
   printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess:\n", theMaxValue);
   
      do 
      {
      scanf("%d",&aGuess);
      if (aGuess < 1)
         {
         printf("You must enter a that's >=1\n");
         continue;
         }
      if (aGuess > theMaxValue)
         {
         printf("You must enter a number that's <= %d\n", theMaxValue);
         continue;
         }
      if (aGuess > theNumberImThinkingOf)
         {
         printf("No cat... the number I'm thinking of is smaller than %d\n",aGuess);
         }   
      if (aGuess < theNumberImThinkingOf)
         {
         printf("No cat... the number I'm thinking of is larger than %d\n",aGuess);
         }   
      if (aGuess == theNumberImThinkingOf)
         {
         printf("You got me, \n");
         printf(" .       . \n");
         printf(" |\\_---_/|\n");
         printf("/   o_o   \\\n"); 
         printf("|    U    |\n");
         printf("\\  ._I_.  /\n");
         printf(" `-_____-'\n");

//         return 0; 
         }
         
      }      

   //while (scanf( "%d", &aGuess ) != theNumberImThinkingOf);
   while (aGuess != theNumberImThinkingOf); 
   return 0;
}

